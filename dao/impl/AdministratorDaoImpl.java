package com.hospital_queuing_call_system.dao.impl;

import com.hospital_queuing_call_system.bean.ScheduleInformation;
import com.hospital_queuing_call_system.dao.AdministratorDao;
import com.hospital_queuing_call_system.utils.JDBCUtils;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by CJBLY on 2023-05-10 21:23
 */
public class AdministratorDaoImpl implements AdministratorDao {

    public AdministratorDaoImpl() {
    }

    /**
     * 管理员登录
     */

    @Override
    public boolean loginIn(String userName, String userPassword) {
        //登录状态
        boolean logonStatus = false;
        Connection connection = JDBCUtils.getConnection();
        //操作数据库
        String sql="select * from administrator_user where username=? and userpassword=?";
        //改变登录方法,使用PreparedStatement实现,可解决sql注入问题
        PreparedStatement preparedStatement = null;
        try {
            //connection.setAutoCommit(false);//事务自动提交关闭
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,userName);
            preparedStatement.setString(2,userPassword);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                logonStatus=true;
            }
            else logonStatus=false;
            //关闭数据库
            JDBCUtils.close(connection,preparedStatement,resultSet);
            return logonStatus;
        } catch (
                SQLException e) {
            throw new RuntimeException(e);
        }
    }


    /**
    *功能描述：添加一条排班信息
    *@param scheduleInformation 排班信息
    *@return
    *           false 添加失败
    *           true 添加成功
    */
    @Override
    public boolean addSchedule(ScheduleInformation scheduleInformation) {

        boolean addStatus = false;

        String addSql = "insert into schedule_information (doctor_name,doctor_number,department,date,time) values (?,?,?,?,?)";
        String doctorName = scheduleInformation.getDoctorName();//医生姓名
        String doctorNumber = scheduleInformation.getDoctorNumber();//医生编号
        String department = scheduleInformation.getDepartment();//科室名称
        Date date = scheduleInformation.getDate();//排班的日期
        String time = scheduleInformation.getTime();//排班的时间（上午、下午）

        Connection connection = JDBCUtils.getConnection();
        PreparedStatement preparedStatement = null;

        try {
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(addSql);
            preparedStatement.setString(1,doctorName);
            preparedStatement.setString(2,doctorNumber);
            preparedStatement.setString(3,department);
            preparedStatement.setDate(4,date);
            preparedStatement.setString(5,time);

            int count = preparedStatement.executeUpdate();

            if(count==1){
                connection.commit();
                System.out.println("添加排班信息成功");
                addStatus = true;
            }else {
                connection.rollback();
                System.out.println("添加排班信息失败");
            }


        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            JDBCUtils.close(connection,preparedStatement,null);
        }

        return addStatus;
    }

    /**
    *功能描述：删除一条排班信息
    *@param scheduleInformation 排班信息
    *@return
    *           false 删除失败
    *           true 删除成功
    */
    @Override
    public boolean deleteSchedule(ScheduleInformation scheduleInformation) {

        boolean deleteStatus = false;

        String deleteSql = "delete from schedule_information where doctor_number=? and date=? and time=?";
        String doctorNumber = scheduleInformation.getDoctorNumber();
        Date date = scheduleInformation.getDate();
        String time = scheduleInformation.getTime();

        Connection connection = JDBCUtils.getConnection();
        PreparedStatement preparedStatement = null;

        try {

            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(deleteSql);
            preparedStatement.setString(1,doctorNumber);
            preparedStatement.setDate(2,date);
            preparedStatement.setString(3,time);

            int count = preparedStatement.executeUpdate();

            if(count == 1){
                connection.commit();
                System.out.println("删除排班信息成功");
                deleteStatus = true;
            }else {
                connection.rollback();
                System.out.println("删除排班信息失败");
            }


        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            JDBCUtils.close(connection,preparedStatement,null);
        }

        return deleteStatus;
    }

    /**
    *功能描述：根据医生姓名查询排班信息
    *@param doctorName 医生姓名
    *@return
    *           ArrayList 排班信息的数组
    */
    @Override
    public ArrayList<ScheduleInformation> selectScheduleByDoctorName(String doctorName) {

        ArrayList<ScheduleInformation> scheduleInformationArrayList = new ArrayList<>();

        ScheduleInformation scheduleInformation = null;

        String selectSql = "select * from schedule_information where doctor_name=?";

        Connection connection = JDBCUtils.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = connection.prepareStatement(selectSql);
            preparedStatement.setString(1,doctorName);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                scheduleInformation = new ScheduleInformation(resultSet.getString(2)
                    ,resultSet.getString(3), resultSet.getString(4)
                    ,resultSet.getDate(5), resultSet.getString(6)
                    ,resultSet.getString(7),resultSet.getInt(8)
                        ,resultSet.getString(9));
                scheduleInformationArrayList.add(scheduleInformation);
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            JDBCUtils.close(connection,preparedStatement,resultSet);
        }


        return scheduleInformationArrayList;
    }

    /**
    *功能描述：根据医生的编号查询排班信息
    *@param doctorNumber
    *@return
    *           ArrayList 排班信息的数组
    */
    @Override
    public ArrayList<ScheduleInformation> selectScheduleByDoctorNumber(String doctorNumber) {
        ArrayList<ScheduleInformation> scheduleInformationArrayList = new ArrayList<>();

        ScheduleInformation scheduleInformation = null;

        String selectSql = "select * from schedule_information where doctor_number=?";

        Connection connection = JDBCUtils.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = connection.prepareStatement(selectSql);
            preparedStatement.setString(1,doctorNumber);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                scheduleInformation = new ScheduleInformation(resultSet.getString(2)
                        ,resultSet.getString(3), resultSet.getString(4)
                        , resultSet.getDate(5), resultSet.getString(6)
                        ,resultSet.getString(7),resultSet.getInt(8)
                        ,resultSet.getString(9));
                scheduleInformationArrayList.add(scheduleInformation);
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            JDBCUtils.close(connection,preparedStatement,resultSet);
        }

        return scheduleInformationArrayList;
    }
}
