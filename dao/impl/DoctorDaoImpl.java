package com.hospital_queuing_call_system.dao.impl;

import com.hospital_queuing_call_system.dao.DoctorDao;
import com.hospital_queuing_call_system.utils.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by CJBLY on 2023-05-10 21:20
 */
public class DoctorDaoImpl implements DoctorDao {

    public DoctorDaoImpl() {
    }

    //医生登录
    @Override
    public boolean loginIn(String userName, String userPassword) {
        //登录状态
        boolean logonStatus = false;
        Connection connection = JDBCUtils.getConnection();
        //操作数据库
        String sql="select * from doctor_user where username=? and userpassword=?";
        //改变登录方法,使用PreparedStatement实现,可解决sql注入问题
        PreparedStatement preparedStatement = null;
        try {
            //connection.setAutoCommit(false);//事务自动提交关闭
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,userName);
            preparedStatement.setString(2,userPassword);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                logonStatus=true;
            }
            else logonStatus=false;
            //关闭数据库
            JDBCUtils.close(connection,preparedStatement,resultSet);
            return logonStatus;
        } catch (
                SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
