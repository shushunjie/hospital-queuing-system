package com.hospital_queuing_call_system.dao;

import com.hospital_queuing_call_system.bean.ScheduleInformation;

import java.util.ArrayList;

/**
 * Created by CJBLY on 2023-05-12 9:19
 */
public interface AdministratorDao {
    public boolean loginIn(String userName, String userPassword);
    boolean addSchedule(ScheduleInformation scheduleInformation);
    public boolean deleteSchedule(ScheduleInformation scheduleInformation);

    public ArrayList<ScheduleInformation> selectScheduleByDoctorName(String doctorName);
    public ArrayList<ScheduleInformation> selectScheduleByDoctorNumber(String doctorNumber);

}
