package com.hospital_queuing_call_system.dao;

/**
 * Created by CJBLY on 2023-05-12 9:18
 */
public interface DoctorDao {
    public boolean loginIn(String userName, String userPassword);
}
