package com.hospital_queuing_call_system.service;

import com.hospital_queuing_call_system.bean.ScheduleInformation;

import java.util.ArrayList;

/**
 * Created by CJBLY on 2023-05-12 9:31
 */
public interface AdministratorService {
    public boolean loginIn(String userName, String userPassword);
    boolean addSchedule(ScheduleInformation scheduleInformation);
    public boolean deleteSchedule(ScheduleInformation scheduleInformation);

    public ArrayList<ScheduleInformation> selectScheduleByDoctorName(String doctorName);
    public ArrayList<ScheduleInformation> selectScheduleByDoctorNumber(String doctorNumber);
}
