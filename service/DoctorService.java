package com.hospital_queuing_call_system.service;

/**
 * Created by CJBLY on 2023-05-12 9:31
 */
public interface DoctorService {
    public boolean loginIn(String userName, String userPassword);
}
