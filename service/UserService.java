package com.hospital_queuing_call_system.service;

import com.hospital_queuing_call_system.bean.*;

import java.sql.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by CJBLY on 2023-05-12 9:31
 */
public interface UserService {
    public boolean loginIn(String userName, String userPassword);
    public boolean register(String userName, String userPassword);

    public Map<String, List<Department>> selectDepartment();

    public List<PaymentInformation> selectPaymentInformationByUserName(String userName);
    public boolean insertPayInformation(PaymentInformation paymentInformation);

    public List<RegisterInformation> selectRegisterInformationByUserName(String userName);
    public boolean insertRegisterInformation(RegisterInformation registerInformation);
    public boolean cancelAppointment(String orderNumber);

    public Patient selectPatientById(String id);
    public List<Patient> selectAllPatients();
    public boolean addPatient(Patient patient);
    public boolean deletePatient(String id);
    public boolean updatePatientPhoneById(String id, String phone);

    public Map<Date,List<ScheduleInformation>> selectScheduleInformation(String department);


}
