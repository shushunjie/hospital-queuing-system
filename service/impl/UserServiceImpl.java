package com.hospital_queuing_call_system.service.impl;

import com.hospital_queuing_call_system.bean.*;
import com.hospital_queuing_call_system.dao.UserDao;
import com.hospital_queuing_call_system.dao.impl.UserDaoImpl;
import com.hospital_queuing_call_system.service.UserService;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CJBLY on 2023-05-12 9:32
 */
public class UserServiceImpl implements UserService {
    private UserDao userDao = new UserDaoImpl();
    public UserServiceImpl() {
    }

    @Override
    public boolean loginIn(String userName, String userPassword){
        boolean logonStatus = false;
        logonStatus = userDao.loginIn(userName,userPassword);
        return logonStatus;
    }

    @Override
    public boolean register(String userName, String userPassword){
        boolean registerStatus = false;
        registerStatus = userDao.register(userName,userPassword);
        return registerStatus;
    }

    @Override
    public Map<String, List<Department>> selectDepartment() {
        Map<String,List<Department>> departmentMap = null;
        departmentMap = userDao.selectDepartment();
        return departmentMap;
    }

    @Override
    public List<PaymentInformation> selectPaymentInformationByUserName(String userName) {
        List<PaymentInformation> paymentInformationList = null;
        paymentInformationList = userDao.selectPaymentInformationByUserName(userName);
        return paymentInformationList;
    }

    @Override
    public boolean insertPayInformation(PaymentInformation paymentInformation) {
        boolean insertStatus = false;
        insertStatus = userDao.insertPayInformation(paymentInformation);
        return insertStatus;
    }

    @Override
    public List<RegisterInformation> selectRegisterInformationByUserName(String userName) {
        List<RegisterInformation> registerInformationList = null;
        registerInformationList = userDao.selectRegisterInformationByUserName(userName);
        return registerInformationList;
    }

    @Override
    public boolean insertRegisterInformation(RegisterInformation registerInformation) {
        boolean insertStatus = false;
        insertStatus = userDao.insertRegisterInformation(registerInformation);
        return insertStatus;
    }

    @Override
    public boolean cancelAppointment(String orderNumber) {
        boolean cancelStatus = false;
        cancelStatus = userDao.cancelAppointment(orderNumber);
        return cancelStatus;
    }

    @Override
    public List<Patient> selectAllPatients() {
        List<Patient> patientList = userDao.selectAllPatients();
        return patientList;
    }

    @Override
    public Patient selectPatientById(String id) {
        Patient patient = null;
        patient = userDao.selectPatientById(id);
        return patient;
    }

    @Override
    public boolean addPatient(Patient patient) {
        boolean addStatus = false;
        addStatus = userDao.addPatient(patient);
        return addStatus;
    }

    @Override
    public boolean deletePatient(String id) {
        boolean deleteStatus = false;
        deleteStatus = userDao.deletePatientById(id);
        return deleteStatus;
    }

    @Override
    public boolean updatePatientPhoneById(String id, String phone) {
        boolean updateStatus = false;
        updateStatus = userDao.updatePatientPhoneById(id,phone);
        return updateStatus;
    }

    @Override
    public Map<Date, List<ScheduleInformation>> selectScheduleInformation(String department) {
        Map<Date,List<ScheduleInformation>> scheduleMap = null;
        scheduleMap = userDao.selectScheduleInformation(department);
        return scheduleMap;
    }

}
