package com.hospital_queuing_call_system.service.impl;

import com.hospital_queuing_call_system.dao.DoctorDao;
import com.hospital_queuing_call_system.dao.impl.DoctorDaoImpl;

/**
 * Created by CJBLY on 2023-05-12 9:32
 */
public class DoctorServiceImpl {
    private DoctorDao doctorDao = new DoctorDaoImpl();

    public DoctorServiceImpl() {
    }

    public boolean loginIn(String userName, String userPassword){
        boolean logonStatus = false;
        logonStatus = doctorDao.loginIn(userName,userPassword);
        return logonStatus;
    }
}
