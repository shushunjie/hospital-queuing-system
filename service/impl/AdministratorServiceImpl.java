package com.hospital_queuing_call_system.service.impl;

import com.hospital_queuing_call_system.bean.ScheduleInformation;
import com.hospital_queuing_call_system.dao.AdministratorDao;
import com.hospital_queuing_call_system.dao.impl.AdministratorDaoImpl;
import com.hospital_queuing_call_system.service.AdministratorService;

import java.util.ArrayList;

/**
 * Created by CJBLY on 2023-05-12 9:32
 */
public class AdministratorServiceImpl implements AdministratorService {
    private AdministratorDao administratorDao = new AdministratorDaoImpl();

    public AdministratorServiceImpl() {
    }

    public boolean loginIn(String userName, String userPassword){
        boolean logonStatus = false;
        logonStatus = administratorDao.loginIn(userName,userPassword);
        return logonStatus;
    }

    @Override
    public boolean addSchedule(ScheduleInformation scheduleInformation) {
        boolean addStatus = false;
        addStatus = administratorDao.addSchedule(scheduleInformation);
        return addStatus;
    }

    @Override
    public boolean deleteSchedule(ScheduleInformation scheduleInformation) {
        boolean deleteStatus = false;
        deleteStatus = administratorDao.deleteSchedule(scheduleInformation);
        return deleteStatus;
    }

    @Override
    public ArrayList<ScheduleInformation> selectScheduleByDoctorName(String doctorName) {
        ArrayList<ScheduleInformation> scheduleInformationArrayList = null;
        scheduleInformationArrayList = administratorDao.selectScheduleByDoctorName(doctorName);
        return scheduleInformationArrayList;
    }

    @Override
    public ArrayList<ScheduleInformation> selectScheduleByDoctorNumber(String doctorNumber) {
        ArrayList<ScheduleInformation> scheduleInformationArrayList = null;
        scheduleInformationArrayList = administratorDao.selectScheduleByDoctorNumber(doctorNumber);
        return scheduleInformationArrayList;
    }
}
