package com.hospital_queuing_call_system.bean;

/**
 * Created by CJBLY on 2023-05-12 9:12
 * 科室类
 */
public class Department {

    private String department;
    private String section;
    private String description;

    public Department() {
    }

    public Department(String department, String section, String description) {
        this.department = department;
        this.section = section;
        this.description = description;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
