package com.hospital_queuing_call_system.bean;

/**
 * hospital_queuing_call_system_demo Created by CJDLY on 2023-05-13 15:36
 */
public class Patient {

    private String userName;//用户账号
    private String name;//就诊人姓名
    private String sex;//就诊人性别
    private int age;//就诊人年龄
    private String phone;//就诊人电话
    private String id;//就诊人身份证

    public Patient() {
    }

    public Patient(String userName, String name, String sex, int age, String phone, String id) {
        this.userName = userName;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.phone = phone;
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
