package com.hospital_queuing_call_system.bean;

/**
 * hospital_queuing_call_system_demo Created by CJDLY on 2023-05-13 10:27
 */

import java.sql.Date;

/**
*挂号信息
*@Version 1.0
*@Author CJDLY
*/
public class RegisterInformation {
    private String orderNumber;//订单编号
    private String registeredStatus;//挂号状态（预约、成功、取消）
    private String registeredLevel;//挂号级别
    private String registeredPayment;//挂号费用
    private String registeredDepartment;//挂号科室
    private String consultDoctorName;//看诊医生姓名
    private String consultDoctorId;//看诊医生的工号
    private String patientVisitorName;//就诊人姓名
    private String patientVisitorSex;//就诊人性别
    private String patientVisitorId;//就诊人身份证
    private Date visitDate;//就诊日期
    private String visitLocation;//就诊地点
    private String userName;//用户账号
    private String visitTime;//就诊时间
    private int patientVisitorAge;//就诊人年龄

    public RegisterInformation() {
    }

    public RegisterInformation(String orderNumber, String registeredStatus, String registeredLevel,
                               String registeredPayment, String registeredDepartment,
                               String consultDoctorName, String consultDoctorId, String patientVisitorName, String patientVisitorSex,
                               String patientVisitorId, Date visitDate, String visitLocation, String userName, String visitTime,int age) {
        this.orderNumber = orderNumber;
        this.registeredStatus = registeredStatus;
        this.registeredLevel = registeredLevel;
        this.registeredPayment = registeredPayment;
        this.registeredDepartment = registeredDepartment;
        this.consultDoctorName = consultDoctorName;
        this.consultDoctorId = consultDoctorId;
        this.patientVisitorName = patientVisitorName;
        this.patientVisitorSex = patientVisitorSex;
        this.patientVisitorId = patientVisitorId;
        this.visitDate = visitDate;
        this.visitLocation = visitLocation;
        this.userName = userName;
        this.visitTime = visitTime;
        this.patientVisitorAge = age;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getRegisteredStatus() {
        return registeredStatus;
    }

    public void setRegisteredStatus(String registeredStatus) {
        this.registeredStatus = registeredStatus;
    }

    public String getRegisteredLevel() {
        return registeredLevel;
    }

    public void setRegisteredLevel(String registeredLevel) {
        this.registeredLevel = registeredLevel;
    }

    public String getRegisteredPayment() {
        return registeredPayment;
    }

    public void setRegisteredPayment(String registeredPayment) {
        this.registeredPayment = registeredPayment;
    }

    public String getRegisteredDepartment() {
        return registeredDepartment;
    }

    public void setRegisteredDepartment(String registeredDepartment) {
        this.registeredDepartment = registeredDepartment;
    }

    public String getConsultDoctorName() {
        return consultDoctorName;
    }

    public void setConsultDoctorName(String consultDoctorName) {
        this.consultDoctorName = consultDoctorName;
    }

    public String getConsultDoctorId() {
        return consultDoctorId;
    }

    public void setConsultDoctorId(String consultDoctorId) {
        this.consultDoctorId = consultDoctorId;
    }

    public String getPatientVisitorName() {
        return patientVisitorName;
    }

    public void setPatientVisitorName(String patientVisitorName) {
        this.patientVisitorName = patientVisitorName;
    }

    public String getPatientVisitorSex() {
        return patientVisitorSex;
    }

    public void setPatientVisitorSex(String patientVisitorSex) {
        this.patientVisitorSex = patientVisitorSex;
    }

    public String getPatientVisitorId() {
        return patientVisitorId;
    }

    public void setPatientVisitorId(String patientVisitorId) {
        this.patientVisitorId = patientVisitorId;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public String getVisitLocation() {
        return visitLocation;
    }

    public void setVisitLocation(String visitLocation) {
        this.visitLocation = visitLocation;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(String visitTime) {
        this.visitTime = visitTime;
    }

    public int getPatientVisitorAge() {
        return patientVisitorAge;
    }

    public void setPatientVisitorAge(int patientVisitorAge) {
        this.patientVisitorAge = patientVisitorAge;
    }
}
