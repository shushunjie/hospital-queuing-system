package com.hospital_queuing_call_system.bean;

/**
 * Created by CJBLY on 2023-05-11 22:33
 */
public class User {
    private String userName;
    private String userPassword;

    public User() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
