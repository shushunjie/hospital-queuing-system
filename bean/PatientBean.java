package com.example.bean;

import java.sql.Time;
import java.util.Objects;

public class PatientBean {


    private int uId;
    private String userName;
    private String name;
    private String sex;
    private int age;
    private String phone;
    private String idCard;
    private Time calledTime;
    public PatientBean(){

    }
    public PatientBean(int uId,String userName,String name,String sex,int age,String phone,String idCard){
        this.uId = uId;
        this.userName = userName;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.phone = phone;
        this.idCard = idCard;
    }

    public int getuId() {
        return uId;
    }

    public void setuId(int uId) {
        this.uId = uId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientBean that = (PatientBean) o;
        return Objects.equals(idCard, that.idCard);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCard);
    }
}
