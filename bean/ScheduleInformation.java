package com.hospital_queuing_call_system.bean;

/**
 * hospital_queuing_call_system_demo Created by CJDLY on 2023-05-24 22:45
 */

import java.sql.Date;

/**
*排班信息
*@Version 1.0
*@Author CJDlY
*/

public class ScheduleInformation {

    private String doctorName;//医生姓名
    private String doctorNumber;//医生编号
    private String department;//科室名称
    private Date date;//排班的日期
    private String time;//排班的时间（上午、下午）
    private String registeredLevel;//挂号级别
    private int number;//余号数量
    private String location;//就诊地点


    public ScheduleInformation() {
    }

    public ScheduleInformation(String doctorName, String doctorNumber, String department, Date date, String time,
                               String registeredLevel, int number,String location) {
        this.doctorName = doctorName;
        this.doctorNumber = doctorNumber;
        this.department = department;
        this.date = date;
        this.time = time;
        this.registeredLevel = registeredLevel;
        this.number = number;
        this.location = location;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDoctorNumber() {
        return doctorNumber;
    }

    public void setDoctorNumber(String doctorNumber) {
        this.doctorNumber = doctorNumber;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRegisteredLevel() {
        return registeredLevel;
    }

    public void setRegisteredLevel(String registeredLevel) {
        this.registeredLevel = registeredLevel;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
