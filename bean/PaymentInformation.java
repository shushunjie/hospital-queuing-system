package com.hospital_queuing_call_system.bean;

/**
* 就诊人的缴费信息
*@Version 1.0
*@Author CJDL
*/
public class PaymentInformation {
    private String userName;
    private String orderNumber;//缴费订单号（主键）
    private String paymentAmount;//缴费金额
    private String patientVisitorName;//就诊人姓名
    private String patientVisitorSex;//就诊人性别
    private int patientVisitorAge;//就诊人年龄
    private String registeredLevel;//就诊人挂号级别
    private String registeredDepartment;//就诊人挂号科室
    private String consultDoctorName;//看诊医生姓名

    public PaymentInformation() {
    }

    public PaymentInformation(String userName, String orderNumber, String paymentAmount,
                              String patientVisitorName, String patientVisitorSex, int patientVisitorAge,
                              String registeredLevel, String registeredDepartment, String consultDoctorName) {
        this.userName = userName;
        this.orderNumber = orderNumber;
        this.paymentAmount = paymentAmount;
        this.patientVisitorName = patientVisitorName;
        this.patientVisitorSex = patientVisitorSex;
        this.patientVisitorAge = patientVisitorAge;
        this.registeredLevel = registeredLevel;
        this.registeredDepartment = registeredDepartment;
        this.consultDoctorName = consultDoctorName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getPatientVisitorName() {
        return patientVisitorName;
    }

    public void setPatientVisitorName(String patientVisitorName) {
        this.patientVisitorName = patientVisitorName;
    }

    public String getPatientVisitorSex() {
        return patientVisitorSex;
    }

    public void setPatientVisitorSex(String patientVisitorSex) {
        this.patientVisitorSex = patientVisitorSex;
    }

    public int getPatientVisitorAge() {
        return patientVisitorAge;
    }

    public void setPatientVisitorAge(int patientVisitorAge) {
        this.patientVisitorAge = patientVisitorAge;
    }

    public String getRegisteredLevel() {
        return registeredLevel;
    }

    public void setRegisteredLevel(String registeredLevel) {
        this.registeredLevel = registeredLevel;
    }

    public String getRegisteredDepartment() {
        return registeredDepartment;
    }

    public void setRegisteredDepartment(String registeredDepartment) {
        this.registeredDepartment = registeredDepartment;
    }

    public String getConsultDoctorName() {
        return consultDoctorName;
    }

    public void setConsultDoctorName(String consultDoctorName) {
        this.consultDoctorName = consultDoctorName;
    }
}
