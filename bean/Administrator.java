package com.hospital_queuing_call_system.bean;

/**
 * Created by CJBLY on 2023-05-11 22:41
 */
public class Administrator {
    private String userName;
    private String userPassword;

    public Administrator() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
