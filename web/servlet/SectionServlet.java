package com.hospital_queuing_call_system.web.servlet;

import com.hospital_queuing_call_system.bean.ScheduleInformation;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import java.util.Map;

/**
 * hospital_queuing_call_system_demo Created by CJDLY on 2023-05-27 9:31
 */
public class SectionServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Date date = Date.valueOf(request.getParameter("date"));
        int number = Integer.parseInt(request.getParameter("number"));
        String price = request.getParameter("price");

        HttpSession httpSession = request.getSession();

        Map<Date, List<ScheduleInformation>> scheduleInformationMap = (Map<Date, List<ScheduleInformation>>)
                httpSession.getAttribute("scheduleInformationMap");

        ScheduleInformation scheduleInformation = scheduleInformationMap.get(date).get(number);

        httpSession.setAttribute("scheduleInformation",scheduleInformation);
        httpSession.setAttribute("price",price);


        System.out.println("SectionServlet: "+request.getSession());
        System.out.println("SectionServlet-request: "+request);
        System.out.println("SectionServlet-servletContext: "+request.getServletContext());


        response.sendRedirect("makeAppointment.jsp");
//        request.getRequestDispatcher("makeAppointment.jsp").forward(request,response);

    }
}
