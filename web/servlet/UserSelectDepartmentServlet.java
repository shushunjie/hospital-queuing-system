package com.hospital_queuing_call_system.web.servlet;

import com.hospital_queuing_call_system.service.UserService;
import com.hospital_queuing_call_system.service.impl.UserServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Created by CJBLY on 2023-05-12 16:29
 */
public class UserSelectDepartmentServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserService userService = new UserServiceImpl();
        //
        request.setAttribute("departmentInfo",userService.selectDepartment());
        request.getRequestDispatcher("user_select_department.jsp").forward(request,response);
    }
}
