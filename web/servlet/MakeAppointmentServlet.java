package com.hospital_queuing_call_system.web.servlet;

import com.hospital_queuing_call_system.bean.Patient;
import com.hospital_queuing_call_system.bean.RegisterInformation;
import com.hospital_queuing_call_system.bean.ScheduleInformation;
import com.hospital_queuing_call_system.service.UserService;
import com.hospital_queuing_call_system.service.impl.UserServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;

/**
 * hospital_queuing_call_system_demo Created by CJDLY on 2023-05-27 18:41
 */
public class MakeAppointmentServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        long time = System.currentTimeMillis();

        String id = request.getParameter("id");


        boolean insertStatus = false;

        HttpSession httpSession = request.getSession();

        UserService userService = new UserServiceImpl();

        Patient patient = userService.selectPatientById(id);

        ScheduleInformation scheduleInformation = (ScheduleInformation) httpSession.getAttribute("scheduleInformation");


        String orderNumber = time +"_"+scheduleInformation.getDate();//订单编号
        String registeredStatus = "预约成功";//挂号状态（预约、成功、取消）
        String registeredLevel = scheduleInformation.getRegisteredLevel();//挂号级别
        String registeredPayment = (String) httpSession.getAttribute("price");//挂号费用
        String registeredDepartment = scheduleInformation.getDepartment();//挂号科室
        String consultDoctorName = scheduleInformation.getDoctorName();//看诊医生姓名
        String consultDoctorId = scheduleInformation.getDoctorNumber();//看诊医生的工号
        String patientVisitorName = patient.getName();//就诊人姓名
        String patientVisitorSex = patient.getSex();//就诊人性别
        String patientVisitorId = patient.getId();//就诊人身份证
        Date visitDate = scheduleInformation.getDate();//就诊日期
        String visitLocation = scheduleInformation.getLocation();//就诊地点
        String userName = (String) httpSession.getAttribute("userName");//用户账号
        String visitTime = scheduleInformation.getTime();//就诊时间
        int patientVisitorAge = patient.getAge();

        RegisterInformation registerInformation = new RegisterInformation(orderNumber,registeredStatus
                ,registeredLevel,registeredPayment,registeredDepartment,consultDoctorName,consultDoctorId,patientVisitorName
                ,patientVisitorSex,patientVisitorId,visitDate,visitLocation
                ,userName,visitTime,patientVisitorAge);

        insertStatus = userService.insertRegisterInformation(registerInformation);

        if(insertStatus){
            request.setAttribute("registerStatus","预约成功");
            request.getRequestDispatcher("makeAppointment.jsp").forward(request,response);
        }else {
            request.setAttribute("registerStatus","预约失败");
        }


    }
}
