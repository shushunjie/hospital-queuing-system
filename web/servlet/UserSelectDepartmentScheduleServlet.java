package com.hospital_queuing_call_system.web.servlet;

import com.hospital_queuing_call_system.service.UserService;
import com.hospital_queuing_call_system.service.impl.UserServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

/**
 * hospital_queuing_call_system_demo Created by CJDLY on 2023-05-25 9:24
 *查询科室排班信息
 */
public class UserSelectDepartmentScheduleServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserService userService = new UserServiceImpl();

        HttpSession httpSession = request.getSession();

        String department = request.getParameter("section");
        String description = request.getParameter("description");

        httpSession.setAttribute("section",department);
        httpSession.setAttribute("description",description);
        httpSession.setAttribute("scheduleInformationMap",userService.selectScheduleInformation(department));

        System.out.println("UserSelectDepartmentScheduleServlet: "+request.getSession());
        System.out.println("UserSelectDepartmentScheduleServlet-request: "+request);
        System.out.println("UserSelectDepartmentScheduleServlet-ServletContext: "+request.getServletContext());

        response.sendRedirect("section.jsp");
//        request.getRequestDispatcher("section.jsp").forward(request,response);

    }
}
