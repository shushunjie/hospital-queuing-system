package com.hospital_queuing_call_system.web.servlet;

import com.hospital_queuing_call_system.dao.AdministratorDao;
import com.hospital_queuing_call_system.dao.DoctorDao;
import com.hospital_queuing_call_system.dao.UserDao;
import com.hospital_queuing_call_system.dao.impl.AdministratorDaoImpl;
import com.hospital_queuing_call_system.dao.impl.DoctorDaoImpl;
import com.hospital_queuing_call_system.dao.impl.UserDaoImpl;
import com.hospital_queuing_call_system.service.UserService;
import com.hospital_queuing_call_system.service.impl.UserServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

/**
 * Created by CJBLY on 2023-05-09 20:31
 */
public class LoginInServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //登录系统
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        request.setCharacterEncoding("UTF-8");

        String identity = request.getParameter("identity");
        String userName = request.getParameter("userName");
        String userPassword = request.getParameter("userPassword");

        HttpSession httpSession = request.getSession();//得到请求游览器(客户端)对应的session。如果没有，那么就创建应该新的

        //登录状态
        boolean logonStatus = false;

        switch (identity){
            case "管理员":
                AdministratorDao administratorDao = new AdministratorDaoImpl();
                logonStatus = administratorDao.loginIn(userName,userPassword);
                if(logonStatus){
                    httpSession.setAttribute("userName",userName);
                    httpSession.setAttribute("userPassword",userPassword);
                    response.sendRedirect("");
                    System.out.println("管理员登录成功");
                }else{
                    response.sendRedirect("");
                    System.out.println("管理员登录失败");
                }
                break;

            case "医生":
                DoctorDao doctorDao = new DoctorDaoImpl();
                logonStatus = doctorDao.loginIn(userName,userPassword);
                if(logonStatus){
                    httpSession.setAttribute("userName",userName);
                    httpSession.setAttribute("userPassword",userPassword);
                    response.sendRedirect("");
                    System.out.println("医生登录成功");
                }else{
                    response.sendRedirect("");
                    System.out.println("医生登录失败");
                }
                break;
            case "用户":
                UserService userService = new UserServiceImpl();
                logonStatus = userService.loginIn(userName,userPassword);
                if(logonStatus){
                    httpSession.setAttribute("userName",userName);
                    httpSession.setAttribute("userPassword",userPassword);
                    httpSession.setAttribute("patientList",userService.selectAllPatients());
//                    response.sendRedirect(request.getContextPath()+"/user_booking.jsp"); 两者都可以
                    response.sendRedirect(request.getContextPath()+"/select_department");
//                    request.getContextPath()  /hospital_queuing_call_system
//                    request.getServletPath() /login
                    System.out.println("用户登录成功");
                }else{
//                    response.sendRedirect("");
                    System.out.println("用户登录失败");
                }
                break;
        }

//        response.sendRedirect("user_booking.jsp");

    }
}
