package com.hospital_queuing_call_system.web.servlet;

import com.hospital_queuing_call_system.dao.impl.UserDaoImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by CJBLY on 2023-05-10 23:58
 */
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        PrintWriter out = response.getWriter();

        String userName = request.getParameter("userName");
        String userPassword = request.getParameter("userPassword");
        String confirmPassword = request.getParameter("confirmPassword");

        if( !userPassword.equals(confirmPassword) ){
            System.out.println(request.getContextPath()+"= ="+request.getServletPath());
            response.sendRedirect(request.getContextPath()+"/");
            out.write("密码不一致");
        }
        else {
            UserDaoImpl userDaoImpl = new UserDaoImpl();
            boolean registerStatus = userDaoImpl.register(userName,userPassword);//注册状态



        }

    }
}
